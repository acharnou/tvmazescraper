﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Models.Maze;

namespace TvMazeScraper.Infrastructure.Cache
{
    public interface IMazeCache
    {
		MazeShowsPage CachePage(int pageNumber, IEnumerable<MazeShow> shows);
	    MazeShowsPage GetCachedPage(int pageNumber);
		MazeShowCastInfo CacheCast(int showId, IEnumerable<MazeCast> casts);
		MazeShowCastInfo GetCachedCast(int showId);
	}
}
