﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Infrastructure.Http
{
    public class HttpRequestor : IHttpRequestor
    {
		private const string _userAgentKey = "User-Agent";
		private const string _userAgentValue = "API Scrapper";


		public async Task<string> LoadUrl(string url)
	    {
		    using (HttpClient client = new HttpClient())
		    {
			    client.DefaultRequestHeaders.Accept.Clear();
			    client.DefaultRequestHeaders.Add(_userAgentKey, _userAgentValue);

			    var requestResult = await client.GetStringAsync(url);

			    return requestResult;
		    }
	    }
	}
}
