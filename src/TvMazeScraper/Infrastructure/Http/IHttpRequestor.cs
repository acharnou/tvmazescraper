﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TvMazeScraper.Infrastructure.Http
{
    public interface IHttpRequestor
    {
	    Task<string> LoadUrl(string url);
    }
}