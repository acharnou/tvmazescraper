﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Parsers
{
    public interface IApiSerializer
    {
	    string Serialize<T>(T entity);
	    T Deserialize<T>(string text);
    }
}