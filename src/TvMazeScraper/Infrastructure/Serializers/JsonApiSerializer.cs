﻿using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure.Parsers;
using Newtonsoft.Json;

namespace Infrastructure.Serializers
{
	public class JsonApiSerializer : IApiSerializer
	{
		public T Deserialize<T>(string text)
		{
			return JsonConvert.DeserializeObject<T>(text);
		}

		public string Serialize<T>(T entity)
		{
			return JsonConvert.SerializeObject(entity);
		}
	}
}