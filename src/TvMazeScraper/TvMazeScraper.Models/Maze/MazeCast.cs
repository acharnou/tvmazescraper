﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using TvMazeScraper.Models.Base;

namespace TvMazeScraper.Models.Maze
{
    public class MazeCast
    {
		public int Id { get; set; }
		public MazePerson Person { get; set; }
    }
}
