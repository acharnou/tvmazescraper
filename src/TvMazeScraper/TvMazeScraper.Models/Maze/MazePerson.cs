﻿using System;
using System.Collections.Generic;
using System.Text;
using TvMazeScraper.Models.Base;

namespace TvMazeScraper.Models.Maze
{
	public class MazePerson : ApiBaseEntity
	{
		public string Birthday { get; set; }
	}
}
