﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TvMazeScraper.Models.Base;

namespace TvMazeScraper.Models.Maze
{
    public class MazeShow : ApiBaseEntity
    {
		[JsonIgnore]
		public IEnumerable<MazeCast> Casts { get; set; }
	}
}
