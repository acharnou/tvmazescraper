﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeScraper.Models.Maze
{
    public class MazeShowCastInfo
    {
		public int Id { get; set; }
		public int ShowId { get; set; }
		public IEnumerable<MazeCast> Casts { get; set; }
	}
}
