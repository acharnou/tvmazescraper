﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeScraper.Models.Maze
{
    public class MazeShowsPage
    {
		public int Id { get; set; }
		public int PageNumber { get; set; }
		public IEnumerable<MazeShow> Shows { get; set; }
    }
}
