﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TvMazeScraper.Models.Base;
using TvMazeScraper.Models.Maze;

namespace TvMazeScraper.Models.Result
{
    public class ResultShow : ApiBaseEntity
    {
		public ResultShow()	{}

		public ResultShow(MazeShow mazeShow)
		{
			Id = mazeShow.Id;
			Name = mazeShow.Name;
			Casts = mazeShow.Casts.Select(cast => cast.Person);
		}

		[JsonProperty("cast")]
		public IEnumerable<MazePerson> Casts { get; set; }
    }
}
