﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TvMazeScraper.Models.Maze;

namespace TvMazeScraper.Services.MazeApi
{
    public interface IMazeApiService
    {
		Task<IEnumerable<MazeShow>> GetShowsByPage(int page);
	    Task<IEnumerable<MazeCast>> GetCastByShow(int showId);
    }
}
