﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Parsers;
using Newtonsoft.Json;
using TvMazeScraper.Infrastructure.Http;
using TvMazeScraper.Models.Maze;

namespace TvMazeScraper.Services.MazeApi
{
	public class MazeApiService : IMazeApiService
	{
		private const string MazeApiUrlBase = "http://api.tvmaze.com/shows{0}";
		private const string MazeApiShowParameter = "?page={0}";
		private const string MazeApiCastParameter = "/{0}/cast";

		private readonly IApiSerializer _serializer;
		private readonly IHttpRequestor _requestor;

		public MazeApiService(IHttpRequestor requestor, IApiSerializer serializer)
		{
			_serializer = serializer;
			_requestor = requestor;
		}

		public async Task<IEnumerable<MazeCast>> GetCastByShow(int showId)
		{
			var castParameter = string.Format(MazeApiCastParameter, showId);
			return await GetRequestResult<IEnumerable<MazeCast>>(castParameter).ConfigureAwait(false);
		}

		public async Task<IEnumerable<MazeShow>> GetShowsByPage(int page)
		{
			var showParameter = string.Format(MazeApiShowParameter, page);
			return await GetRequestResult<IEnumerable<MazeShow>>(showParameter).ConfigureAwait(false);
		}

		private async Task<T> GetRequestResult<T>(string requestParameters)
		{
			var requestUrl = string.Format(MazeApiUrlBase, requestParameters);
			var loadResult = await _requestor.LoadUrl(requestUrl).ConfigureAwait(false);
			return _serializer.Deserialize<T>(loadResult);
		}
	}
}
