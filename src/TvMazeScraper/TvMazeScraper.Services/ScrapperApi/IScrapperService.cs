﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TvMazeScraper.Models.Maze;
using TvMazeScraper.Models.Result;

namespace TvMazeScraper.Services.ScrapperApi
{
    public interface IScrapperService
    {
		Task<IEnumerable<ResultShow>> GetShows(int page);
	}
}
