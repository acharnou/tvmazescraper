﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Infrastructure.Cache;
using TvMazeScraper.Models.Maze;
using TvMazeScraper.Models.Result;
using TvMazeScraper.Services.MazeApi;

namespace TvMazeScraper.Services.ScrapperApi
{
    public class ScrapperService : IScrapperService
    {
		private const int _mazePageLimit = 250;
		private const int _apiPageLimit = 25;
		private const int _pagesRelationCount = _mazePageLimit / _apiPageLimit;

		private readonly IMazeCache _mazeCache;
		private readonly IMazeApiService _mazeApiService;

		public ScrapperService(IMazeCache mazeCache, IMazeApiService mazeApiService)
		{
			_mazeCache = mazeCache;
			_mazeApiService = mazeApiService;
		}

		public async Task<IEnumerable<ResultShow>> GetShows(int page)
		{
			var mazePage = CalculateMazePage(page);

			// Get cached or remote shows
			var cachedPage = _mazeCache.GetCachedPage(mazePage);
			if (cachedPage == null)
			{
				var shows = await _mazeApiService.GetShowsByPage(mazePage);
				cachedPage = _mazeCache.CachePage(mazePage, shows);
			}

			// Take only needed shows
			var neededPage =  (page % _pagesRelationCount) * _apiPageLimit;
			var finalShows = cachedPage.Shows.Skip(neededPage).Take(_apiPageLimit);

			// Get cached or remote casts
			foreach (var show in finalShows)
			{
				var showId = show.Id;

				var cachedCast = _mazeCache.GetCachedCast(showId);
				if (cachedCast == null)
				{
					var cast = await _mazeApiService.GetCastByShow(showId);
					cachedCast = _mazeCache.CacheCast(showId, cast);
				}

				show.Casts = cachedCast.Casts;
			}

			var convertedResult = ConvertToResultShows(finalShows);

			return convertedResult;
		}

		private int CalculateMazePage(int page)
		{
			var mazePage = Math.Floor(((page + 1) * _apiPageLimit) / (double)_mazePageLimit);

			return (int)mazePage;
		}

		private IEnumerable<ResultShow> ConvertToResultShows(IEnumerable<MazeShow> mazeShows)
		{
			return mazeShows.Select(sh => new ResultShow(sh));
		}
	}
}