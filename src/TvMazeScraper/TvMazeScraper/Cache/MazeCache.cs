﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TvMazeScraper.Infrastructure.Cache;
using TvMazeScraper.Models.Maze;

namespace TvMazeScraper.Cache
{
	/// <summary>
	/// Serves as a cache for shows and cast information
	/// </summary>
	public class MazeCache : IMazeCache
	{
		private readonly MazeShowContext _showContext;

		public MazeCache(MazeShowContext context)
		{
			_showContext = context;
		}

		public MazeShowsPage CachePage(int pageNumber, IEnumerable<MazeShow> shows)
		{
			MazeShowsPage result = null;

			var cachedPage = _showContext.MazeShowItems.FirstOrDefault(p => p.PageNumber == pageNumber);
			if (cachedPage == null)
			{
				var pageToAdd = new MazeShowsPage()
				{
					PageNumber = pageNumber,
					Shows = shows
				};

				_showContext.MazeShowItems.Add(pageToAdd);
				_showContext.SaveChanges();

				result = pageToAdd;
			}

			return result;
		}

		public MazeShowsPage GetCachedPage(int pageNumber)
		{
			return _showContext.MazeShowItems.FirstOrDefault(p => p.PageNumber == pageNumber);
		}

		public MazeShowCastInfo CacheCast(int showId, IEnumerable<MazeCast> casts)
		{
			MazeShowCastInfo result = null;

			var cachedCast = _showContext.MazeShowCastItems.FirstOrDefault(p => p.ShowId == showId);
			if (cachedCast == null)
			{
				var castToAdd = new MazeShowCastInfo()
				{
					ShowId = showId,
					Casts = casts
				};

				_showContext.MazeShowCastItems.Add(castToAdd);
				_showContext.SaveChanges();

				result = castToAdd;
			}

			return result;
		}

		public MazeShowCastInfo GetCachedCast(int showId)
		{
			return _showContext.MazeShowCastItems.FirstOrDefault(p => p.ShowId == showId);
		}
	}
}
