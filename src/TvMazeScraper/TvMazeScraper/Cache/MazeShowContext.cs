﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TvMazeScraper.Models.Maze;

namespace TvMazeScraper.Cache
{
	public class MazeShowContext : DbContext
	{
		public MazeShowContext(DbContextOptions<MazeShowContext> options)
			: base(options)
		{
		}

		public DbSet<MazeShowsPage> MazeShowItems { get; set; }
		public DbSet<MazeShowCastInfo> MazeShowCastItems { get; set; }
	}
}
