﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TvMazeScraper.Cache;
using TvMazeScraper.Models.Maze;
using TvMazeScraper.Models.Result;
using TvMazeScraper.Services.MazeApi;
using TvMazeScraper.Services.ScrapperApi;

namespace TvMazeScraper.Controllers
{
	[Route("api/[controller]")]
	public class ShowsController : Controller
	{
		private readonly MazeShowContext _context;
		private readonly IScrapperService _scrapperService;

		public ShowsController(MazeShowContext context, IScrapperService scrapperService)
		{
			_context = context;
			_scrapperService = scrapperService;
		}

		// GET api/shows
		[HttpGet]
		public ActionResult Get([FromQuery]int page = 0)
		{
			// Note: general exceptions in production will be handled by specific /error action

			if (page < 0)
			{
				return NotFound();
			}

			var mazeShows = _scrapperService.GetShows(page).Result;


			if (mazeShows == null || !mazeShows.Any())
			{
				return NoContent();
			}

			return Ok(mazeShows);
		}

		[Route("/Error")]
		public IActionResult Index()
		{
			// Handle error here
			// TODO: Add errors handling
			return null;
		}
	}
}
