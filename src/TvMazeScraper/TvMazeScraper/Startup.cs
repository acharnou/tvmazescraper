﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Parsers;
using Infrastructure.Serializers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TvMazeScraper.Cache;
using TvMazeScraper.Infrastructure.Cache;
using TvMazeScraper.Infrastructure.Http;
using TvMazeScraper.Services.MazeApi;
using TvMazeScraper.Services.ScrapperApi;

namespace TvMazeScraper
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
	        services.AddDbContext<MazeShowContext>(opt =>
		        opt.UseInMemoryDatabase("MazePages"), ServiceLifetime.Singleton);

	        services.AddSingleton<IApiSerializer, JsonApiSerializer>();
			services.AddSingleton<IHttpRequestor, HttpRequestor>();
			services.AddSingleton<IMazeApiService, MazeApiService>();
	        services.AddSingleton<IMazeCache, MazeCache>();
			services.AddSingleton<IScrapperService, ScrapperService>();

			services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
			app.UseStatusCodePages();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/error");
			}

            app.UseMvc();
        }
    }
}
